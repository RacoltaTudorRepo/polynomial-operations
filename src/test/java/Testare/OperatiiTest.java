package Testare;
import static org.junit.Assert.*;
import Control.*;
import Model.*;
import org.junit.Test;
import java.util.*;
public class OperatiiTest {
	ArrayList<Monom> monoame1= new ArrayList<Monom>();
	ArrayList<Monom> monoame2=new ArrayList<Monom>();
	Polinom poli1;
	Polinom poli2;
	Control ctrl= new Control();
	@Test
	public void test() {
		monoame1.add(new Monom(3,1));
		monoame1.add(new Monom(2,2));
		monoame1.add(new Monom(1,1));
		monoame1.add(new Monom(0,1));
		poli1=new Polinom(monoame1);
		
		monoame2.add(new Monom(1,1));
		monoame2.add(new Monom(0,3));
		poli2=new Polinom(monoame2);
		
		Polinom adunare=new Polinom(ctrl.adunare(poli1,poli2).getTermeni());
		System.out.println(adunare.afisare());
		String adunareS="X^0*(4.0)+X^1*(2.0)+X^2*(2.0)+X^3*(1.0)";
		
		assertTrue(adunare.afisare().equals(adunareS));
	}

}
