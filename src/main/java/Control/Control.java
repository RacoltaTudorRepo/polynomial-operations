package Control;
import Model.*;
import java.util.ArrayList;

public class Control {
	public Polinom adunare(Polinom poli1,Polinom poli2) {
		ArrayList<Monom> rezultat= new ArrayList<Monom>();
		ArrayList<Monom> temp1= (ArrayList<Monom>) poli1.getTermeni().clone();
		ArrayList<Monom> temp2= (ArrayList<Monom>) poli2.getTermeni().clone();
		for(Monom M1: poli1.getTermeni())
			for(Monom M2: poli2.getTermeni())
					if(M1.getP()==M2.getP())
						{rezultat.add(new Monom(M1.getP(),M1.getC()+M2.getC()));
						temp1.remove(M1);
						temp2.remove(M2);
						}
		for(Monom M1: temp1)
			rezultat.add(M1);
		for(Monom M2: temp2)
			rezultat.add(M2);
		//if(rezultat.get(rezultat.size()-1).getC()==0)
			//rezultat.remove(rezultat.size()-1);
		rezultat.sort(null);
		Polinom result= new Polinom(rezultat);
		return result;
	}
	public Polinom scadere(Polinom poli1,Polinom poli2) {
		if (poli2.getTermeni().get(poli2.getTermeni().size()-1).getC()==0)
		{ Polinom result= new Polinom(poli1.getTermeni());
		return result;
		}
		ArrayList<Monom> rezultat1= new ArrayList<Monom>();
		ArrayList<Monom> temp1= (ArrayList<Monom>) poli1.getTermeni().clone();
		ArrayList<Monom> temp2= (ArrayList<Monom>) poli2.getTermeni().clone();
		for(Monom M1: poli1.getTermeni())
			for(Monom M2: poli2.getTermeni())
					if(M1.getP()==M2.getP())
						{rezultat1.add(new Monom(M1.getP(),M1.getC()-M2.getC()));
						temp1.remove(M1);
						temp2.remove(M2);
						}
		for(Monom M1: temp1)
			rezultat1.add(new Monom(M1.getP(),M1.getC()));
		for(Monom M2: temp2)
			{double x=0-M2.getC();
			rezultat1.add(new Monom(M2.getP(),x));
			}
		//if(rezultat1.get(rezultat1.size()-1).getC()==0)
			//rezultat1.remove(rezultat1.size()-1);
		rezultat1.sort(null);
		Polinom result= new Polinom(rezultat1);
		return result;
	}
	
	public Polinom inmultire(Polinom poli1,Polinom poli2) {
		ArrayList<Monom> rezultat= new ArrayList<Monom>();
		ArrayList<Monom> finalul= new ArrayList<Monom>();
		for(Monom M1: poli1.getTermeni())
			for(Monom M2: poli2.getTermeni())
				rezultat.add(new Monom(M1.getP()+M2.getP(),M1.getC()*M2.getC())); //inmultire fiecare cu fiecare
		ArrayList<Monom> temp= (ArrayList<Monom>) rezultat.clone();
		for(Monom M1:temp)
			for(Monom M2:temp)
				if(temp.indexOf(M1)<temp.indexOf(M2)) //restrangere rezultat
				if(M1.getP()==M2.getP() && M1.getC()!=M2.getC()) 
					{finalul.add(new Monom(M1.getP(),M1.getC()+M2.getC())); //se va face adunarea pe monoamele de acelasi grad
					rezultat.remove(M1); //le vom elimina dupa ce sunt procesate
					rezultat.remove(M2);
					}
		for (Monom M:rezultat)
			finalul.add(M); //monoamele ramase in rezultat se vor adauga la solutia finala
		finalul.sort(null);
		Polinom result= new Polinom(finalul);
		return result;
	}
	
	public Polinom derivare(Polinom poli1) {
		ArrayList<Monom> rezultat= new ArrayList<Monom>();
		for (Monom M: poli1.getTermeni())
			if(M.getP()!=0)
			rezultat.add(new Monom(M.getP()-1,M.getC()*M.getP())); //derivarea fiecarui monom
		Polinom pol = new Polinom(rezultat);
		return pol;
	}
	
	public Polinom integrare(Polinom poli1) {
		ArrayList<Monom> rezultat= new ArrayList<Monom>();
		for (Monom M: poli1.getTermeni())
				rezultat.add(new Monom(M.getP()+1,M.getC()/(M.getP()+1))); //integrare nedefinita clasica pe fiecare monom
		Polinom pol = new Polinom(rezultat);
		return pol;
	}
	
	public Polinom impartire(Polinom poli1, Polinom poli2) {
		if (poli2.getTermeni().get(poli2.getTermeni().size()-1).getP()==0 && poli2.getTermeni().get(poli2.getTermeni().size()-1).getC()==0)
			return poli2;
		if(poli2.getTermeni().get(poli2.getTermeni().size()-1).getP()==0)
			{ArrayList<Monom> rezultat= new ArrayList<Monom>();
			for(Monom M1:poli1.getTermeni())
				rezultat.add(new Monom(M1.getP(),M1.getC()/poli2.getTermeni().get(poli2.getTermeni().size()-1).getC()));
			Polinom pol = new Polinom(rezultat);
			return pol;
			}
		if(poli2.getTermeni().get(poli2.getTermeni().size()-1).getP()>poli1.getTermeni().get(poli1.getTermeni().size()-1).getP())
			{ArrayList<Monom> rezultat= new ArrayList<Monom>();
			rezultat.add(new Monom(-1,-1));
			Polinom pol =new Polinom(rezultat);
			return pol;
			}
		ArrayList<Monom> rezultat= new ArrayList<Monom>();
		Polinom temp2=new Polinom(poli1.getTermeni());
		while(temp2.getTermeni().get(temp2.getTermeni().size()-1).getP()-poli2.getTermeni().get(poli2.getTermeni().size()-1).getP()!=-1)
			{rezultat.add(new Monom(temp2.getTermeni().get(temp2.getTermeni().size()-1).getP()-poli2.getTermeni().get(poli2.getTermeni().size()-1).getP(),(temp2.getTermeni().get(temp2.getTermeni().size()-1).getC())/poli2.getTermeni().get(poli2.getTermeni().size()-1).getC()));
			ArrayList<Monom> help= new ArrayList<Monom>();
			help.add(new Monom(temp2.getTermeni().get(temp2.getTermeni().size()-1).getP()-poli2.getTermeni().get(poli2.getTermeni().size()-1).getP(),(temp2.getTermeni().get(temp2.getTermeni().size()-1).getC())/poli2.getTermeni().get(poli2.getTermeni().size()-1).getC()));
			Polinom intermediarContinue= new Polinom(help); //se va face un polinom care contine monomul pus in rezultat
			Polinom temp3=inmultire(intermediarContinue,poli2); //inmultirea lui cu impartitorul
			//System.out.println("\nTEMP 3");S
			//temp3.afisare();
			for(Monom M: temp3.getTermeni())
				M.negCoef();
			//System.out.println(" ");
			//temp3.afisare();
			Polinom temp4=adunare(temp2,temp3); //prin negare, putem aduna, altfel puteam scadea direct
			temp4.getTermeni().sort(null);
			temp4.getTermeni().remove(temp4.getTermeni().size()-1); //eliminam monomul cu coef 0 obtinut prin adunare
			//System.out.println("\nTEMP 4");
			//temp4.afisare();
			temp2=new Polinom(temp4.getTermeni()); //repetare algoritm 
			//temp2.afisare();
			}
		rezultat.sort(null);
		if(temp2.getTermeni().get(temp2.getTermeni().size()-1).getC()!=0) //adaugare rest la rezultat
			for(Monom M2:temp2.getTermeni())
				rezultat.add(M2);
		Polinom pol = new Polinom(rezultat);
		return pol;
	}
}
