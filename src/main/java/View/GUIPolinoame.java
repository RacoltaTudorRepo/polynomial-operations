package View;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class GUIPolinoame {

	private JFrame frame;
	private JTextField textpolinom1;
	private JTextField textpolinom2;
	private JTextArea textArea;
	
	private String polinomul1=null;
	private String polinomul2=null;
	private String rezultat=null;
	
	public GUIPolinoame() {
		frame = new JFrame();
		frame.setBounds(100, 100, 511, 516);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textpolinom1 = new JTextField();
		textpolinom1.setBounds(100, 30, 260, 20);
		frame.getContentPane().add(textpolinom1);
		textpolinom1.setColumns(10);
		
		textpolinom2 = new JTextField();
		textpolinom2.setBounds(100, 80, 260, 20);
		frame.getContentPane().add(textpolinom2);
		textpolinom2.setColumns(10);
		
		JLabel label1 = new JLabel("Polinom 1");
		label1.setBounds(20, 30, 60, 15);
		frame.getContentPane().add(label1);
		
		JLabel label2 = new JLabel("Polinom 2");
		label2.setBounds(20, 80, 60, 15);
		frame.getContentPane().add(label2);
		
		JLabel introducere = new JLabel("Se va introduce sub forma: (putere,coeficient)");
		introducere.setBounds(120, 120, 300, 15);
		frame.getContentPane().add(introducere);
		
		JButton rezultatBTN = new JButton("Rezultat");
		rezultatBTN.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textArea.append(rezultat);
			}
		});
		rezultatBTN.setBounds(200, 150, 100, 25);
		frame.getContentPane().add(rezultatBTN);
		
		JButton btnEnter = new JButton("Enter");
		btnEnter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				polinomul1=textpolinom1.getText();
			}
		});
		
		btnEnter.setBounds(370, 30, 90, 25);
		frame.getContentPane().add(btnEnter);
		
		JButton btnEnter_1 = new JButton("Enter");
		btnEnter_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				polinomul2=textpolinom2.getText();
			}
		});
		btnEnter_1.setBounds(370, 80, 90, 25);
		frame.getContentPane().add(btnEnter_1);
		
		textArea = new JTextArea();
		frame.getContentPane().add(textArea);
		textArea.setBounds(10, 200, 470, 230);
	}
	public void showResult(String result) {
		JOptionPane.showMessageDialog(frame,result);
	}
	
	public JFrame getFrame() {
		return frame;
	}
	
	public String getPol1() {
		return polinomul1;
	}
	
	public String getPol2() {
		return polinomul2;
	}
	public JTextArea getTA() {
		return textArea;
	}
	public void setRezultat(String rezultat) {
		this.rezultat=rezultat;
	}
}
