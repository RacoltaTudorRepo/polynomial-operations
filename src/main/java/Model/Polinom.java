package Model;
import Control.*;
import java.util.*;
public class Polinom{
	public Control ctrl;
	private ArrayList<Monom> termeni;
	public Polinom(ArrayList<Monom> termeni) {
		this.termeni=termeni;
	}
	
	public String afisareImpartire() {
		String rezultat="";
		if(this.getTermeni().get(this.getTermeni().size()-1).getP()==-1)
			{rezultat="-";
			return rezultat;
			}
		int last=termeni.size()-1;
		for(int i=0;i<termeni.size()-1;i++)
			{if(termeni.get(last).getC()!=0)
				rezultat=rezultat+"X^"+termeni.get(i).getP()+"*("+termeni.get(i).getC()+")+";
			else rezultat=rezultat+"X^"+termeni.get(i).getP()+"*("+termeni.get(i).getC()+")";
			if (i>=0)
				if(termeni.get(i+1).getP()<=termeni.get(i).getP())
					{if(i==last-1)
						{rezultat=rezultat+"Rest: X^"+termeni.get(i+1).getP()+"*("+termeni.get(i+1).getC()+")";
						return rezultat;
						}
					else {rezultat=rezultat+"Rest: X^"+termeni.get(i+1).getP()+"*("+termeni.get(i+1).getC()+")+";
					i++;
						}
					}
			}
		if(termeni.get(last).getC()!=0) rezultat=rezultat+"X^"+ termeni.get(last).getP() + "*("+termeni.get(last).getC()+")";
		return rezultat;
	} 
	
	public String afisare() {
		String rezultat="";
		if(this.getTermeni().get(this.getTermeni().size()-1).getP()==-1)
			{rezultat="-";
			return rezultat;
			}
		int last=termeni.size()-1;
		for(int i=0;i<termeni.size()-1;i++)
			{if(termeni.get(last).getC()!=0)
				rezultat=rezultat+"X^"+termeni.get(i).getP()+"*("+termeni.get(i).getC()+")+";
			else rezultat=rezultat+"X^"+termeni.get(i).getP()+"*("+termeni.get(i).getC()+")";
			}
		if(termeni.get(last).getC()!=0) rezultat=rezultat+"X^"+ termeni.get(last).getP() + "*("+termeni.get(last).getC()+")";
		return rezultat;
	}
	public ArrayList<Monom> getTermeni(){
		return termeni;
	}
}
