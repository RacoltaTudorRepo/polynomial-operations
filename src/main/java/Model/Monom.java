package Model;

public class Monom implements Comparable<Monom>{
	private double coef;
	private int putere;
	public Monom(int putere,double coef) {
		this.putere=putere;
		this.coef=coef;
	}
	public double getC() {
		return coef;
	}
	public int getP() {
		return putere;
	}
	public void negCoef() {
		this.coef=0-this.coef;
	}
	public int compareTo(Monom m2) {
		if(this.putere==m2.getP()) return 0;
		else if(this.putere<m2.getP()) return -1;
		else return 1;
	}
}
