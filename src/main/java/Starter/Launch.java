package Starter;
import java.util.ArrayList;
import View.*;
import Control.*;
import Model.*;

public class Launch {
	public static void main(String[] args) throws Exception{
			ArrayList<Monom> temp= new ArrayList<Monom>();
			ArrayList<Monom> temp2= new ArrayList<Monom>();
			
			Control ctrl=new Control();
			GUIPolinoame gui=new GUIPolinoame();
			gui.getFrame().setVisible(true);
			String text=gui.getPol1();
			
			while(text==null)
				{text= gui.getPol1();
				System.out.print("");
				}
			
			int coef = 0;
			int putere=0;
			String[] splitare=text.split(",()");
			for(String aux:splitare)
				{if(aux.charAt(0)=='(')
					putere=Character.getNumericValue(aux.charAt(1));
				if(aux.charAt(1)==')')
					{coef=Character.getNumericValue(aux.charAt(0));
					temp.add(new Monom(putere,coef));
					}
				if(aux.length()==3)
					{coef=0-Character.getNumericValue(aux.charAt(1));
					temp.add(new Monom(putere,coef));
					}
				}
			temp.sort(null);
			Polinom polinom1=new Polinom(temp);
			
			String text1=gui.getPol2();
			while(text1==null)
				{text1= gui.getPol2();
				System.out.print("");
				}
			
			int coef1 = 0;
			int putere1=0;
			String[] splitare1=text1.split(",()");
			for(String aux:splitare1)
				{if(aux.charAt(0)=='(')
					putere1=Character.getNumericValue(aux.charAt(1));
				if(aux.charAt(1)==')')
					{coef1=Character.getNumericValue(aux.charAt(0));
					temp2.add(new Monom(putere1,coef1)); //trebuie tratat coeficientii negativi!!!
					}
				if(aux.length()==3)
				{coef1=0-Character.getNumericValue(aux.charAt(1));
				temp2.add(new Monom(putere1,coef1));
				}
				}
			
			temp2.sort(null);
			Polinom polinom2=new Polinom(temp2);
			System.out.println(polinom1.afisare());
			System.out.println(polinom2.afisare());
			
			 Polinom adunarea=ctrl.adunare(polinom1,polinom2);
			 Polinom inmultirea=ctrl.inmultire(polinom1,polinom2);
			 Polinom scaderea=ctrl.scadere(polinom1,polinom2);
			 Polinom impartirea=ctrl.impartire(polinom1,polinom2);
			 Polinom derivarea=ctrl.derivare(polinom1);
			 Polinom integrarea=ctrl.integrare(polinom1);
			 String rezultatInmultire=inmultirea.afisare();
			 String rezultatImpartire=impartirea.afisare();
			 if(impartirea.afisareImpartire().equals(""))
				 rezultatImpartire="Nu s-a putut efectua impartire la polinom nul!";
			 else if(impartirea.afisareImpartire().equals("-"))
				 rezultatImpartire="Rangul impartitorului este mai mare ca deimpartitului!";
			 if(rezultatInmultire.charAt(5)=='0')
				 rezultatInmultire="-";
				 
			 
			 String deAfisat="Adunarea celor 2 :"+adunarea.afisare()+"\n\n"+
			 "Inmultirea celor 2 :"+rezultatInmultire+"\n\n"+
			 "Scaderea celor 2 :"+scaderea.afisare()+"\n\n"+
			 "Impartirea celor 2 :"+rezultatImpartire+"\n\n"+
			 "Derivarea primului :"+derivarea.afisare()+"\n\n"+
			 "Integrarea primului :"+integrarea.afisare()+"\n\n";
			 
			 gui.setRezultat(deAfisat);
			 //gui.getTA().append(adunarea.afisare());
			 //gui.getTA().append(inmultirea.afisare());
	}
}
